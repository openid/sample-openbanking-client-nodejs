A sample client for the UK/Brazil OpenBanking APIs for node.js

Based on various code fragments/help received from Dave Tonge at Moneyhub
(thanks!) and others.

This is a very simple example that only calls the accounts request and accounts
API, and it mainly an example of implementing the security profile correctly.

OpenID Foundation use this to test the conformance suite, and others can use it to
see how the conformance suite is used to test relying parties.

**This code is not complete and is not suitable for use in production.**

See the wiki for instructions:

https://gitlab.com/openid/conformance-suite/-/wikis/Users/How-to-run-OpenBanking-UK-RP-(client)-tests

Only Node.js LTS versions ^12.19.0 (Erbium), ^14.15.0 (Fermium), and ^16.13.0 (Gallium) are supported.
