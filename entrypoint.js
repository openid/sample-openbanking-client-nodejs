/* eslint camelcase: 0 */
const config = require("./config.json");
const getClient = require("./client");
const tlsOpts = require("./tls-opts");
const tlsOptsBrazilClient1 = require("./tls-opts_brazil_client_1");
const tlsOptsBrazilClient2 = require("./tls-opts_brazil_client_2");

const got = require("got");
const R = require("ramda");
const URL = require("url").URL;
const url = require("url");
const jwtDecode = require("jwt-decode");
const { generators } = require("openid-client");
const { v4: uuidv4 } = require("uuid");
const idempotencyKey = uuidv4();
require("dotenv").load();

// To enable http logging, set NODE_DEBUG environment variable to 'http'

require("https").globalAgent.options.ca = require("ssl-root-cas").create();
require("https").globalAgent.options.rejectUnauthorized = false; // FIXME

const getHeaders = () => ({
    "x-fapi-financial-id": "0015800000jf8aKAAQ",
    // "x-fapi-customer-last-logged-time": new Date(
    //   Date.now() - 30 * 60 * 1000
    // ).toUTCString(),
    // "x-fapi-customer-ip-address": "89.233.114.212",
    "x-fapi-interaction-id": uuidv4(),
});

if (process.env.CONFORMANCE_SERVER === undefined) {
    throw new Error(
        "Required environment variables not set. Please see https://gitlab.com/openid/conformance-suite/-/wikis/Users/How-to-run-OpenBanking-UK-RP-(client)-tests and/or https://gitlab.com/openid/conformance-suite/-/wikis/Brazil-RP-Testing-Instructions-and-Example-Configuration for more information."
    );
}

const accountRequestUrl = new URL(
    `${process.env.CONFORMANCE_SERVER}${process.env.ACCOUNT_REQUEST}`
);
const testModuleName = process.env.TEST_MODULE_NAME;

const authRequestMethod = process.env.FAPI_AUTH_REQUEST_METHOD;
const fapiResponseMode = process.env.FAPI_RESPONSE_MODE;
const pkceCodeVerifier = generators.codeVerifier();
const pkceCodeChallenge = generators.codeChallenge(pkceCodeVerifier);
let fapiClientType = process.env.FAPI_CLIENT_TYPE;
if (fapiClientType === undefined) { // Use previous name prior to rename
    fapiClientType = process.env.FAPI_JARM_TYPE;
}
const fapiProfile = process.env.FAPI_PROFILE;
let brazilClientScope = process.env.BRAZIL_CLIENT_SCOPE;
if (brazilClientScope) {
    brazilClientScope = brazilClientScope.replace("-", " ");
}
let isBrazilPayments = false;
if (brazilClientScope && brazilClientScope.indexOf("payments") > -1) {
    isBrazilPayments = true;
}

let resourceUrl = "";
let brazilConsentRequestUrl = "";
let brazilConsentRequestData = {};

let brazilPaymentInitiationRequestData = {
    localInstrument: "DICT",
    payment: {
        amount: "1.12",
        currency: "BRL",
    },
    creditorAccount: {
        ispb: "12345678",
        issuer: "1774",
        number: "1234567890",
        accountType: "CACC",
    },
    remittanceInformation: "Test",
    //"qrCode": "",
    proxy: "12345678901",
    cnpjInitiator: "50681111000000",
    transactionIdentification: "E00011111000002222233y6j6",
};

MTLS_FQDN=process.env.CONFORMANCE_SERVER;
if (process.env.CONFORMANCE_SERVER_MTLS){
    MTLS_FQDN=process.env.CONFORMANCE_SERVER_MTLS;
    if (!MTLS_FQDN.endsWith("/")){
        MTLS_FQDN = MTLS_FQDN + "/";
    }
}

if (fapiProfile === "openbanking_brazil") {
    if (isBrazilPayments) {
        resourceUrl = new URL(
            `${MTLS_FQDN}${process.env.BRAZIL_PAYMENT_INIT_ENDPOINT}`
        );
        brazilConsentRequestUrl = new URL(
            `${MTLS_FQDN}${process.env.BRAZIL_PAYMENTS_CONSENT_REQUEST}`
        );
        brazilConsentRequestData = {
            data: {
                loggedUser: {
                    document: {
                        identification: "11111111111",
                        rel: "CPF",
                    },
                },
                creditor: {
                    personType: "PESSOA_JURIDICA",
                    cpfCnpj: "11111111111111",
                    name: "John Doe",
                },
                payment: {
                    type: "PIX",
                    date: "2021-08-16",
                    currency: "BRL",
                    amount: "1.12",
                },
            },
        };
    } else {
        resourceUrl = new URL(
            `${MTLS_FQDN}${process.env.BRAZIL_ACCOUNTS_ENDPOINT}`
        );
        brazilConsentRequestUrl = new URL(
            `${MTLS_FQDN}${process.env.BRAZIL_CONSENT_REQUEST}`
        );
        brazilConsentRequestData = {
            data: {
                loggedUser: {
                    document: {
                        identification: "11111111111",
                        rel: "CPF",
                    },
                },
                businessEntity: {
                    document: {
                        identification: "11111111111111",
                        rel: "CNPJ",
                    },
                },
                permissions: ["ACCOUNTS_READ"],
            },
        };
    }
} else {
    resourceUrl = new URL(
        `${MTLS_FQDN}${process.env.ACCOUNTS}`
    );
}

let brazilConsentId = "";

////////////////Global config variables and functions that return configuration options
const accountRequestData = {
    Data: {
        Permissions: ["ReadAccountsBasic"],
    },
    Risk: {},
};

function getRequestObjectClaimsClaimForBrazil() {
    let rv = {
        id_token: {
            cpf: {
                essential: true,
                values: ["11111111111"],
            },
            cnpj: {
                essential: true,
                values: ["11111111111111"],
            },
            acr: {
                essential: true,
            },
        },
        userinfo: {
            cpf: {
                essential: true,
                values: ["11111111111"],
            },
            cnpj: {
                essential: true,
                values: ["11111111111111"],
            },
            acr: {
                essential: true,
            },
        },
    };
    return rv;
}

function getRequestObjectClaimsClaimDefault(accountRequestId) {
    let rv = {
        id_token: {
            openbanking_intent_id: {
                value: accountRequestId,
                essential: true,
            },
            acr: {
                essential: true,
                values: ["urn:openbanking:psd2:sca", "urn:openbanking:psd2:ca"],
            },
        },
    };

    return rv;
}

function testRequiresIdTokenEncryption() {
    let testsThatRequireEncryption = [
        "fapi1-advanced-final-client-test-encrypted-idtoken",
        "fapi1-advanced-final-client-test-encrypted-idtoken-usingrsa15",
    ];
    return testsThatRequireEncryption.includes(testModuleName);
}

function testIsRefreshTokenTest() {
    return testModuleName === "fapi1-advanced-final-client-refresh-token-test";
}

function testRequiresRequestObjectEncryption() {
    if (
        fapiProfile === "openbanking_brazil" &&
        authRequestMethod !== "pushed"
    ) {
        return true;
    }
    if (
        testModuleName ===
        "fapi1-advanced-final-client-encrypted-requestobject-test"
    ) {
        return true;
    }
    return false;
}

/////////////////////END OF CONFIG RELATED STUFF

////////////////////////functions
/**
 * Used for OB UK
 */
function setupAccountRequest(token) {
    console.log("accountRequestUrl=" + accountRequestUrl);
    const requestOptions = {
        https: {
            certificate: tlsOpts.cert,
            key: tlsOpts.key,
            certificateAuthority: tlsOpts.ca,
            ca: tlsOpts.ca,
            rejectUnauthorized: tlsOpts.rejectUnauthorized,
        },
        json: accountRequestData,
        responseType: "json",
        headers: {
            Authorization: `Bearer ${token}`,
            Accept: "application/json; charset=utf-8",
            "Content-Type": "application/json; charset=utf-8",
            ...getHeaders(),
        },
        timeout: tlsOpts.timeout,
    };
    return got
        .post(accountRequestUrl, requestOptions)
        .catch((err) => {
            console.log("Error from " + accountRequestUrl);
            console.log(err);
        });
}

/**
 * Always called at the end
 */
async function callResourceUrl(client, token) {
    console.log("callResourceUrl: " + resourceUrl);
    if (isBrazilPayments) {
        console.log("Calling Brazil payment initiation endpoint");
        let requestJson = {
            data: [ brazilPaymentInitiationRequestData ],
            aud: resourceUrl,
            jti: uuidv4(),
            iss: "2b602d87-d061-470c-89d6-ecc8b163260e", //<- must match the org in client cert
        };
        let paymentsRequestJwt = await client.requestObject(
            {
                ...requestJson,
            },
            {
                sign: "PS256",
            }
        );
        const requestOptions = {
            https: {
                certificate: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.cert
                    : tlsOptsBrazilClient1.cert,
                key: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.key
                    : tlsOptsBrazilClient1.key,
                certificateAuthority: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.ca
                    : tlsOptsBrazilClient1.ca,
                ca: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.ca
                    : tlsOptsBrazilClient1.ca,
                rejectUnauthorized: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.rejectUnauthorized
                    : tlsOptsBrazilClient1.rejectUnauthorized,
            },
            body: paymentsRequestJwt,
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: "application/jwt",
                "Content-Type": "application/jwt",
                "x-idempotency-key": idempotencyKey,
                ...getHeaders(),
            },
            timeout: tlsOpts.timeout,
        };
        // console.log(requestOptions.body);
        // console.log(requestOptions.headers);
        let response = await got
            .post(resourceUrl, requestOptions)
            .catch((err) => {
                console.log("Error from " + resourceUrl);
                console.log(err);
            });
        if (!response) {
            // console.log("Resource endpoint returned an error:");
            // console.log(response);
            return response;
        }
        let decoded = jwtDecode(response.body);
        // console.log("-----PAYMENT INITIATION RESPONSE---");
        // console.log(decoded);
        return decoded;
    } else if (fapiProfile === "openbanking_brazil") {
        //accounts
        console.log("Calling Brazil consents endpoint (accounts)");
        const requestOptions = {
            https: {
                certificate: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.cert
                    : tlsOptsBrazilClient1.cert,
                key: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.key
                    : tlsOptsBrazilClient1.key,
                certificateAuthority: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.ca
                    : tlsOptsBrazilClient1.ca,
                ca: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.ca
                    : tlsOptsBrazilClient1.ca,
                rejectUnauthorized: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.rejectUnauthorized
                    : tlsOptsBrazilClient1.rejectUnauthorized,
            },
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: "application/json; charset=utf-8",
                "Content-Type": "application/json; charset=utf-8",
                ...getHeaders(),
            },
            responseType: "json",
        };
        let response = await got
            .post(resourceUrl, requestOptions)
            .catch((err) => {
                console.log("Error from " + resourceUrl);
                console.log(err);
            });
        return response;
    } else {
        const requestOptions = {
            https: {
                certificate: tlsOpts.cert,
                key: tlsOpts.key,
                certificateAuthority: tlsOpts.ca,
                rejectUnauthorized: tlsOpts.rejectUnauthorized,
            },
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: "application/json; charset=utf-8",
                "Content-Type": "application/json; charset=utf-8",
                ...getHeaders(),
            },
            responseType: "json",
        };
        let response = await got
            .post(resourceUrl, requestOptions)
            .catch((err) => {
                console.log("Error from " + resourceUrl);
                console.log(err);
            });
        return response;
    }
}

/**
 * Calls brazil consent endpoint
 */
async function brazilRequestConsent(client, token) {
    if (isBrazilPayments) {
        console.log(
            "brazilRequestConsent (payments): " + brazilConsentRequestUrl
        );
        let requestJson = brazilConsentRequestData;
        let paymentsRequestJwt = await client.requestObject(
            {
                ...requestJson,
                iss: "2b602d87-d061-470c-89d6-ecc8b163260e",
                aud: brazilConsentRequestUrl,
                jti: uuidv4(),
            },
            {
                sign: "PS256",
            }
        );

        const requestOptions = {
            https: {
                certificate: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.cert
                    : tlsOptsBrazilClient1.cert,
                key: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.key
                    : tlsOptsBrazilClient1.key,
                certificateAuthority: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.ca
                    : tlsOptsBrazilClient1.ca,
                ca: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.ca
                    : tlsOptsBrazilClient1.ca,
                rejectUnauthorized: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.rejectUnauthorized
                    : tlsOptsBrazilClient1.rejectUnauthorized,
            },
            body: paymentsRequestJwt,
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: "application/jwt",
                "Content-Type": "application/jwt",
                "x-idempotency-key": idempotencyKey,
                ...getHeaders(),
            },
            timeout: tlsOpts.timeout,
        };
        // console.log("Brazil payments consent request data:");
        // console.log(brazilConsentRequestData);
        // console.log(requestOptions.headers);

        let response = await got
            .post(brazilConsentRequestUrl, requestOptions)
            .catch((err) => {
                console.log("Error from " + brazilConsentRequestUrl);
                console.log(err);
            });

        let decoded = jwtDecode(response.body);
        // console.log("Decoded Brazil consent response:");
        // console.log(decoded);
        return decoded["data"];
    } else {
        console.log(
            "brazilRequestConsent (accounts): " + brazilConsentRequestUrl
        );
        const requestOptions = {
            https: {
                certificate: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.cert
                    : tlsOptsBrazilClient1.cert,
                key: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.key
                    : tlsOptsBrazilClient1.key,
                certificateAuthority: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.ca
                    : tlsOptsBrazilClient1.ca,
                ca: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.ca
                    : tlsOptsBrazilClient1.ca,
                rejectUnauthorized: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.rejectUnauthorized
                    : tlsOptsBrazilClient1.rejectUnauthorized,
            },
            json: brazilConsentRequestData,
            responseType: "json",
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: "application/json",
                "Content-Type": "application/json",
                ...getHeaders(),
            },
            timeout: tlsOpts.timeout,
        };
        let response = await got
            .post(brazilConsentRequestUrl, requestOptions)
            .catch((err) => {
                console.log("Error from " + brazilConsentRequestUrl);
                console.log(err);
            });
        return response.body["data"];
    }
}

/**
 * get a Brazil consent which was created by brazilRequestConsent
 */
async function brazilGetConsentRequest(token, client) {
    const brazilGetConsentRequestUrl = new URL(
        brazilConsentRequestUrl + "/" + brazilConsentId
    );
    if (isBrazilPayments) {
        console.log(
            "brazilGetConsentRequest (payments): " + brazilGetConsentRequestUrl
        );
        const requestOptions = {
            https: {
                certificate: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.cert
                    : tlsOptsBrazilClient1.cert,
                key: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.key
                    : tlsOptsBrazilClient1.key,
                certificateAuthority: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.ca
                    : tlsOptsBrazilClient1.ca,
                ca: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.ca
                    : tlsOptsBrazilClient1.ca,
                rejectUnauthorized: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.rejectUnauthorized
                    : tlsOptsBrazilClient1.rejectUnauthorized,
            },
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: "application/jwt",
                ...getHeaders(),
            },
            timeout: tlsOpts.timeout,
        };
        let response = await got
            .get(brazilGetConsentRequestUrl, requestOptions)
            .catch((err) => {
                console.log("Error from " + brazilConsentRequestUrl);
                console.log(err);
            });

        let decoded = jwtDecode(response.body);
        // console.log("Decoded Get Consent Response:");
        // console.log(decoded);
        return decoded["data"]["status"];
    } else {
        console.log(
            "brazilGetConsentRequest (accounts): " + brazilGetConsentRequestUrl
        );
        const requestOptions = {
            https: {
                certificate: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.cert
                    : tlsOptsBrazilClient1.cert,
                key: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.key
                    : tlsOptsBrazilClient1.key,
                certificateAuthority: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.ca
                    : tlsOptsBrazilClient1.ca,
                ca: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.ca
                    : tlsOptsBrazilClient1.ca,
                rejectUnauthorized: client.id_token_encrypted_response_alg
                    ? tlsOptsBrazilClient2.rejectUnauthorized
                    : tlsOptsBrazilClient1.rejectUnauthorized,
            },
            responseType: "json",
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: "application/json",
                ...getHeaders(),
            },
            timeout: tlsOpts.timeout,
        };
        let response = await got
            .get(brazilGetConsentRequestUrl, requestOptions)
            .catch((err) => {
                console.log("Error from " + brazilGetConsentRequestUrl);
                console.log(err);
            });
        return response["body"]["data"]["status"];
    }
}

async function getAuthUrl({
    client,
    accountRequestId,
    state,
    nonce,
    response_type,
}) {
    let claims = {};
    if (fapiProfile === "openbanking_brazil") {
        claims = getRequestObjectClaimsClaimForBrazil();
    } else {
        //TODO another if block will be needed for Australia and probably others
        claims = getRequestObjectClaimsClaimDefault(accountRequestId);
    }

    let authParams = {};
    if (fapiProfile === "openbanking_brazil") {
        authParams = {
            client_id: config.client_id,
            state,
            scope: brazilClientScope + " consent:" + brazilConsentId,
            nonce,
            response_type,
            redirect_uri: config.redirect_uri,
        };
        console.log("authParams for Brazil");
        console.log(authParams);
    } else {
        authParams = {
            client_id: config.client_id,
            state,
            scope: "openid accounts",
            nonce,
            response_type,
            redirect_uri: config.redirect_uri,
        };
        if (fapiClientType == "plain_oauth") {
            authParams.scope = "accounts";
        }
        console.log("authParams");
        console.log(authParams);
    }

    if (authRequestMethod == "pushed") {
        authParams["code_challenge"] = pkceCodeChallenge;
        authParams["code_challenge_method"] = "S256";
    }

    let requestObjectOptions = {
        sign: "PS256",
    };
    if (testRequiresRequestObjectEncryption()) {
        //TODO this applies to all, not only Brazil
        requestObjectOptions.encrypt = {
            alg: "RSA-OAEP",
            enc: "A256GCM",
        };
    }

    var exp = Date.now() + 300 * 1000;
    exp = Math.floor(exp / 1000);

    return client
        .requestObject(
            R.merge(authParams, {
                claims,
                exp,
            }),
            requestObjectOptions
        )
        .then(R.assoc("request", R.__, authParams))
        .then((params) => client.authorizationUrl(params));
}

async function authorize(authUrl, otherOptions, client) {
    let parsedAuthUrl = url.parse(authUrl, true);

    const { pathname, query } = parsedAuthUrl;
    let response;
    if (authRequestMethod == "pushed") {
        console.log("Sending PAR request:" + JSON.stringify(query));
        const parResponse = await client.pushedAuthorizationRequest({
            client_id: query.client_id,
            request: query.request,
        });
        console.log("PAR response:" + JSON.stringify(parResponse));
        const { request_uri } = parResponse;

        let newAuthUrl =
            parsedAuthUrl.protocol +
            "//" +
            parsedAuthUrl.host +
            pathname +
            "?request_uri=" +
            encodeURIComponent(request_uri) +
            "&client_id=" +
            encodeURIComponent(client.client_id);
        console.log("authentication request to", newAuthUrl);

        response = await got(newAuthUrl, { followRedirect: false });
    } else {
        console.log("authentication request to", pathname);
        console.log(
            "authentication request parameters",
            JSON.stringify(query, null, 4)
        );
        response = await got(authUrl, otherOptions);
    }
    const { query: callback } = url.parse(
        response.headers.location.replace("#", "?"),
        true
    );
    console.log("authentication response", JSON.stringify(callback, null, 4));
    return response;
}

async function authorizationCallback(
    client,
    redirectUri,
    authorizationResponseParams,
    checks
) {
    try {
        if (authRequestMethod == "pushed") {
            checks["code_verifier"] = pkceCodeVerifier;
        }
        if (fapiResponseMode == "jarm") {
            checks["jarm"] = true;
            client.authorization_signed_response_alg = "PS256";
        }
        console.log("authentication callback checks:" + JSON.stringify(checks));
        let res;
        if (fapiClientType == "plain_oauth") {
            res = await client.oauthCallback(
                redirectUri,
                authorizationResponseParams,
                checks
            );
        } else {
            res = await client.callback(
                redirectUri,
                authorizationResponseParams,
                checks
            );
        }

        console.log(
            "authentication callback succeeded",
            JSON.stringify(res, null, 4)
        );
        return res;
    } catch (err) {
        console.log("authentication callback failed", err);
        throw err;
    }
}

async function main() {
    try {
        let clientConfig = {
            ...config,
            token_endpoint_auth_signing_alg: "PS256",
            id_token_signed_response_alg: "PS256",
            authorization_signed_response_alg: "PS256",
        };
        if (testRequiresIdTokenEncryption()) {
            clientConfig.id_token_encrypted_response_alg = "RSA-OAEP";
            clientConfig.id_token_encrypted_response_enc = "A256GCM";
        }

        const client = await getClient(
            clientConfig,
            process.env.ISSUER,
            fapiProfile
        );
        // allow 60 seconds clock skew between our host and the server to
        // account for possible small differences in device clocks
        client.CLOCK_TOLERANCE = 60;
        // console.log("Loaded client:");
        // console.log(client);

        let authUrl = "";
        let clientCredentialsAccessToken = null;

        //TODO how response type is detected is potentially problematic
        const response_type = client.openbankingclient_response_type_to_use;
        const state = "test-state";
        const nonce = "123abcdef";

        if (fapiProfile === "openbanking_uk") {
            const token = await client.grant({
                grant_type: "client_credentials",
                scope: "accounts",
            });

            console.log(
                "got client credentials access token: " + token.access_token
            );

            const data = await setupAccountRequest(token.access_token);
            clientCredentialsAccessToken = token.access_token;

            accountRequestId = data.body["Data"]["AccountRequestId"];
            console.log("accountRequestId = " + accountRequestId);

            authUrl = await getAuthUrl({
                client,
                state,
                nonce,
                response_type,
                accountRequestId,
            });
        } else if (fapiProfile === "openbanking_brazil") {
            console.log("Calling token endpoint with client credentials");
            const token = await client.grant({
                grant_type: "client_credentials",
                scope: isBrazilPayments ? "payments" : "consents",
            });

            console.log(
                "got client credentials access token: " + token.access_token
            );

            const data = await brazilRequestConsent(client, token.access_token);
            clientCredentialsAccessToken = token.access_token;

            brazilConsentId = data.consentId;
            console.log("brazilConsentId = " + brazilConsentId);

            authUrl = await getAuthUrl({
                client,
                state,
                nonce,
                response_type,
            });
        } else {
            //plain_fapi
            authUrl = await getAuthUrl({
                client,
                state,
                nonce,
                response_type,
            });
        }

        console.log("authorization url is: " + authUrl);

        const authorizationResponse = await authorize(
            authUrl,
            { followRedirect: false },
            client
        );

        const urlObject = new URL(authorizationResponse.headers.location);

        if (client.openbankingclient_response_type_to_use === "code") {
            var params = client.callbackParams(
                authorizationResponse.headers.location
            );
        } else if (
            client.openbankingclient_response_type_to_use === "code id_token"
        ) {
            var hash = urlObject.hash;
            if (hash != null) {
                if (hash.startsWith("#")) {
                    var new_url = "";
                    new_url = "?" + hash.substr(1);
                    params = client.callbackParams(new_url);
                    const encodedIdToken = await client.decryptIdToken(
                        params["id_token"]
                    ); //decryptIdToken is no-op if not encrypted
                    const decodedIdToken = jwtDecode(encodedIdToken);

                    const receivedIat = decodedIdToken["iat"];
                    const clientIat = Math.ceil(Date.now() / 1000);
                    const timeDiff = clientIat - receivedIat;
                    //https://openid.net/specs/openid-connect-core-1_0.html#IDTokenValidation
                    //Section 3.1.3.7-10
                    //The iat Claim can be used to reject tokens that were issued too far away from the current time,
                    //limiting the amount of time that nonces need to be stored to prevent attacks.
                    //The acceptable range is Client specific. So we have decided to allow for 5 minutes.
                    const tolerance = client.CLOCK_TOLERANCE + 5 * 60;
                    if (timeDiff > tolerance) {
                        throw new Error("iat is too far in the past");
                    }

                    if (!decodedIdToken["s_hash"]) {
                        throw new Error(
                            "s_hash is missing from the the id_token from the authorization endpoint"
                        );
                    }

                    if (fapiProfile === "openbanking_uk") {
                        const openbankingIntentId =
                            decodedIdToken["openbanking_intent_id"];
                        if (openbankingIntentId !== accountRequestId) {
                            throw new Error(
                                "openbanking_intent_id returned in id_token from authorization endpoint does not match the value sent in the request object"
                            );
                        }
                    }
                } else {
                    throw new Error(
                        "Malformed fragment (should start with a #)"
                    );
                }
            } else {
                throw new Error("Url does not contain fragment");
            }
        } else {
            throw new Error("Server does not support response_type ");
        }

        if (fapiProfile === "openbanking_brazil") {
            //just checking to see if it has changed properly
            const consentStatusAfterAuthz = await brazilGetConsentRequest(
                clientCredentialsAccessToken,
                client
            );
            console.log(
                "Consent status after authorization. status must have changed to 'AUTHORISED' ---> :" +
                    consentStatusAfterAuthz
            );
        }

        const tokens = await authorizationCallback(
            client,
            config.redirect_uri,
            params,
            { response_type, state, nonce }
        );

        console.log("--tokens from authorizationCallback--");
        // console.log(tokens);

        const access_token = tokens.access_token;

        console.log(
            "access token from authorizationCallback is: " + access_token
        );

        let response = await callResourceUrl(client, access_token);

        if (!response && testIsRefreshTokenTest()) {
            //401 error is expected for the refresh token test. if we get a 401, we need to run a refresh token grant and retry with the new access token
            if (tokens.refresh_token) {
                const refreshedTokenset = await client.grant({
                    refresh_token: tokens.refresh_token,
                    grant_type: "refresh_token",
                });
                // console.log("Refreshed tokens");
                // console.log(refreshedTokenset);
                const refreshedTokenset2 = await client.grant({
                    refresh_token: refreshedTokenset.refresh_token,
                    grant_type: "refresh_token",
                });
                // console.log("Second set of refreshed tokens");
                // console.log(refreshedTokenset);
                const refreshedAccessToken = refreshedTokenset2.access_token;
                // console.log(
                //     "refreshed access token is: " + refreshedAccessToken
                // );
                response = await callResourceUrl(client, refreshedAccessToken);
                // console.log(
                //     "COMPLETE: resource endpoint response is: " + JSON.stringify(response)
                // );
            } else {
                console.log(
                    "ERROR: this test requires a refresh_token but one was not issued by the suite, this might be an error in the test"
                );
            }
        } else if (response) {
            console.log("COMPLETE: resource endpoint response: ");
            // console.log(response);
        } else {
            console.log("ERROR: resource endpoint returned an error");
            // console.log(response);
        }
    } catch (e) {
        console.log(e);
    }
}

main();
