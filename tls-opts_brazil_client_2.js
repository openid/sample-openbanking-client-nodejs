const fs = require("fs");
const path = require("path");

const p = (arg) => path.join(__dirname, arg);

module.exports = {
    cert: fs.readFileSync(p("./keys/transport_brazil_client_2.crt")),
    key: fs.readFileSync(p("./keys/transport_brazil_client_2.key")),
    ca: fs.readFileSync(p("./keys/chain_brazil_client_2.crt")),
    rejectUnauthorized: false,
    timeout: 25000,
};
