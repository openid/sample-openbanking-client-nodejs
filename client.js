/* eslint camelcase:0 */
const { Issuer, custom } = require("openid-client");
const tlsOpts = require("./tls-opts");
const tlsOptsBrazilClient1 = require("./tls-opts_brazil_client_1");
const tlsOptsBrazilClient2 = require("./tls-opts_brazil_client_2");

custom.setHttpOptionsDefaults({
    timeout: 15000, // 15 seconds
});

module.exports = async (clientConfig, issuerUrl, fapiProfile) => {
    const nodeIssuer = await Issuer.discover(issuerUrl);
    var method = "";
    if (
        nodeIssuer.metadata.token_endpoint_auth_methods_supported == null ||
        nodeIssuer.metadata.token_endpoint_auth_methods_supported.includes(
            "client_secret_basic"
        )
    ) {
        method = "client_secret_basic";
    } else if (
        nodeIssuer.metadata.token_endpoint_auth_methods_supported.includes(
            "private_key_jwt"
        )
    ) {
        method = "private_key_jwt";
    } else if (
        nodeIssuer.metadata.token_endpoint_auth_methods_supported.includes(
            "tls_client_auth"
        )
    ) {
        method = "tls_client_auth";
    } else {
        throw new Error("Server does not support authentication method");
    }

    var responseType = "";
    if (nodeIssuer.metadata.response_types_supported.includes("code")) {
        responseType = "code";
    }
    if (
        nodeIssuer.metadata.response_types_supported.includes("code id_token")
    ) {
        responseType = "code id_token";
    }
    clientConfig.tls_client_certificate_bound_access_tokens = true;
    clientConfig.token_endpoint_auth_method = method;
    clientConfig.openbankingclient_response_type_to_use = responseType;
    if (fapiProfile === "openbanking_brazil") {
        // new security profile requires that encryption is always used
        clientConfig.id_token_encrypted_response_alg = "RSA-OAEP";
        clientConfig.id_token_encrypted_response_enc = "A256GCM";
    }
    let keys = {};
    if (clientConfig.id_token_encrypted_response_alg) {
        if (fapiProfile === "openbanking_brazil") {
            console.log("Using Brazil client 2 keys");
            keys = require("./keys/keystore_client2_brazil.json");
        } else {
            console.log("Using client 2 keys");
            keys = require("./keys/keystore_client2.json");
        }
    } else {
        if (fapiProfile === "openbanking_brazil") {
            console.log("Using Brazil client 1 keys");
            keys = require("./keys/keystore_client1_brazil.json");
        } else {
            console.log("Using client 1 (non-Brazil) keys");
            keys = require("./keys/keystore_client1.json");
        }
    }

    let fapiClient = new nodeIssuer.FAPI1Client(clientConfig, keys);

    if (fapiProfile === "openbanking_brazil") {
        //tests encrypted id tokens
        if (clientConfig.id_token_encrypted_response_alg) {
            fapiClient[custom.http_options] = () => ({
                cert: tlsOptsBrazilClient2.cert,
                key: tlsOptsBrazilClient2.key,
                ca: tlsOptsBrazilClient2.ca,
            });
        } else {
            fapiClient[custom.http_options] = () => ({
                cert: tlsOptsBrazilClient1.cert,
                key: tlsOptsBrazilClient1.key,
                ca: tlsOptsBrazilClient1.ca,
            });
        }
    } else {
        fapiClient[custom.http_options] = () => ({
            cert: tlsOpts.cert,
            key: tlsOpts.key,
            ca: tlsOpts.ca,
        });
    }
    return fapiClient;
};
