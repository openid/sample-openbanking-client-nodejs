const fs = require("fs");
const path = require("path");

const p = (arg) => path.join(__dirname, arg);

module.exports = {
    cert: fs.readFileSync(p("./keys/transport.crt")),
    key: fs.readFileSync(p("./keys/transport.key")),
    ca: fs.readFileSync(p("./keys/chain.crt")),
    rejectUnauthorized: false,
    timeout: 25000,
};
